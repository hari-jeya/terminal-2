<a name="readme-top"></a>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/hari-jeya/terminal-2">
    <img src="assets/logo-max.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Terminal</h3>

  <p align="center">
    terminal style personal web site
    <br />
    <a href="https://gitlab.com/hari-jeya/terminal-2"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://hari.jeyakrishnan.fr">View Demo</a>
    ·
    <a href="https://gitlab.com/hari-jeya/terminal-2/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/hari-jeya/terminal-2/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#To Do">To Do</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
# About The Project

[![Screen Shot][product-screenshot]](https://hari.jeyakrishnan.fr)

Hello, welcome to the repo of my website.
I co-built this site because like many of you I'm a big fan of the Linux and Open source world.

I wanted to share my love for linux through this site which not only looks like a linux terminal but also has some functionality.

<div align="right">(<a href="#readme-top">back to top</a>)</div>



## Built With

* ![ts][typescript]
* ![css][css]
* ![js][js]


<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- GETTING STARTED -->
# Getting Started
## Terminal site

To get a local copy up and running, follow these simple steps.

### **clone the repo vit SSH (recommended)**
* bash 
  ```sh
  $ git clone git@gitlab.com:hari-jeya/terminal-2.git
  ```
*or via HTTP (doesn't really matter)*
* bash 
  ```sh
  $ git clone https://gitlab.com/hari-jeya/terminal-2.git
  ```
### **Go to the cloned directory and install all dependencies**
* npm
  ```sh
  $ npm install
  ```
### **Build the web site**
* npm
  ```sh
  $ npm run build
  ```
### **Launch a local server for local viewing of the web site**
* npm
  ```sh
  $ npm start
  ```

### PRODUCTION MODE

* bash
  ```sh
  $ npm run export
  ```
This will create export the site in production mode in the folder **out**


---
## BLOG part (Docx)
### **install jekyll**
* bash
  ```sh
  $ sudo apt-get install ruby-full build-essential zlib1g-dev
  
  $ echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc
  $ echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc
  $ echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
  $ source ~/.bashrc

  $ gem install jekyll bundler
  ```

### **go to Dcox folder**
* bash
  ```sh
  $ cd Docx
  ```
### **Build the site and make it available on a local serve**
* bash
  ```sh
  $ bundle exec jekyll serve
  ```

### Build the site in production mode
* bash
  ```sh
  $ JEKYLL_ENV=production bundle exec jekyll b
  ```
This command will build the site and out put the content to a folder called **_site** in the **docx** folder

<!-- To Do -->
## To Do

- [x] Radme
- [x] Themes
- [ ] Integrate blog
- [ ] Export prod version

See the [open issues](https://gitlab.com/hari-jeya/terminal-2/issues) for a full list of proposed features (and known issues).

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".

The use of tags or prefixes in commit messages can help organize the workflow and make it easier to read. here are recommanded tags 
  - **[Feat]**: For new features added.
  - **[Fix]**: For bug fixes.
  - **[Refactor]**: For code changes that neither add a feature nor fix a bug, but improve structure, readability, or performance.
  - **[Docs]**: For documentation updates.
  - **[Style]**: For style-related changes, such as spaces, indentation, etc.
  - **[Test]**: For additions or modifications to tests.
  - **[Chore]**: For general maintenance tasks, such as dependency updates, configuration, etc.
  - **[WIP]**: For work in progress when you want to temporarily save your changes without finalizing them.


I did not build the site from scratch, this project is fork of the repo of [Yassine Fathi](https://github.com/m4tt72)
Don't forget to give the [original project](https://github.com/m4tt72/terminal) a star! Thanks again!

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<div align="right">(<a href="#readme-top">back to top</a>)</div>


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[product-screenshot]: assets/docs/site.png

[typescript]: https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white

[css]: https://img.shields.io/badge/CSS-239120?&style=for-the-badge&logo=css3&logoColor=white

[js]: https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black