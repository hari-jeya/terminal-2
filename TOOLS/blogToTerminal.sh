#!/bin/bash
# ce script sert a exporter le blog en mode production et place le contenue générer dans le dossier docx du site terminal

# Vérifier si le script est exécuté depuis le dossier contenant le script
script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
current_folder=$(pwd)

if [[ "$script_dir" != "$current_folder" ]]; then
    echo "Déplacement vers le dossier contenant le script..."
    cd "$script_dir"
fi

echo "Script exécuté depuis : $(pwd)"
echo "Exécution du script..."

#se placer dans le dossier docx
cd ../Docx
rm -rf _site

#exporter le blog
JEKYLL_ENV=production bundle exec jekyll b

#copier le blog dans le dossier de du terminal app
cp -